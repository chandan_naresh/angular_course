import { CartItem } from "./cartitem.model";

export interface Cart {
  items: CartItem[];
}
