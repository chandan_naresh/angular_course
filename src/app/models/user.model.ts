import { Cart } from "./cart.model";

export interface User {
  uid?: string;
  email: string;
  mobile: string;
  fullName: string;
  isLoggedIn: boolean;
  cart?: Cart;
  password?: string;
  type?: string;
}
