export interface CartItem {
  name: string;
  id: string;
  price: number;
  description: string;
}
