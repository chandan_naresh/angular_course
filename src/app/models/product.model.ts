export interface Product {
  id?: string;
  name: string;
  description: string;
  rate: number;
  createdBy?: string;
  createdOn?: any;
  isVisible: boolean;
  qtyAvailable?: number;
  imageUrl?: string;
}
