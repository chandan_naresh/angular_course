import { Component } from "@angular/core";
import { User } from "./models/user.model";
import { CartItem } from "./models/cartitem.model";
import { Cart } from "./models/cart.model";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AuthService } from "./auth/auth.service";
import { DbService } from "./db.service";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  isLoggedIn: boolean = false;
  constructor(private authSvc: AuthService, private db: DbService) {
    this.authSvc.isAuthenticate().subscribe((res) => {
      if (res) {
        console.log("user logged in");
        this.db.getDocById("users", res.uid).subscribe((r: User) => {
          this.authSvc.currentUser = r;
        });
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
        console.log("no user were found");
      }
    });
  }
}
