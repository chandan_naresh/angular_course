import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: "root",
})
export class DbService {
  constructor(private afs: AngularFirestore) {}

  saveDoc(collectionName: string, doc: any) {
    return this.afs.collection(collectionName).doc(doc.uid).set(doc);
  }

  getDocById(collectionName: string, docId: string) {
    return this.afs.collection(collectionName).doc(docId).valueChanges();
  }
}
