import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { User } from "../models/user.model";
import { DbService } from "../db.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(private firebaseAuth: AngularFireAuth, private db: DbService) {}

  private _currentUser: User;

  isAuthenticate() {
    return this.firebaseAuth.authState;
  }

  public get currentUser(): User {
    return this._currentUser;
  }

  public set currentUser(v: User) {
    this._currentUser = v;
  }

  facebookLogin() {}

  googleLogin() {}

  emailLogin(email: string, password: string) {
    return this.firebaseAuth.auth.signInWithEmailAndPassword(email, password);
  }

  signout() {
    return this.firebaseAuth.auth.signOut().then(() => {
      this.currentUser = null;
    });
  }

  registerUser(user: User) {
    return new Promise((resolve, reject) => {
      this.firebaseAuth.auth
        .createUserWithEmailAndPassword(user.email, user.password)
        .then(
          (r) => {
            user.uid = r.user.uid;
            this.db.saveDoc("users", user).then(
              (r) => {
                resolve(r);
              },
              (err) => reject(err)
            );
          },
          (err) => reject(err)
        );
    });
    // return this.db.saveDoc("users", user);
  }

  resetPassword() {}
}
