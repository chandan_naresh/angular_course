import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { SellerLoginComponent } from "./seller-login/seller-login.component";

const routes: Routes = [
  { path: "", component: LoginComponent },
  { path: "signup", component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class AuthRoutingModule {}
