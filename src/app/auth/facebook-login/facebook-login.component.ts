import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-facebook-login",
  template: `<button type="submit" class="btn btn-primary">
    Facebook Login
  </button> `,
  styleUrls: ["./facebook-login.component.scss"],
})
export class FacebookLoginComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
