import { Component, OnInit } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { auth } from "firebase";
import { Router } from "@angular/router";
@Component({
  selector: "app-google-login",
  templateUrl: "./google-login.component.html",
  styleUrls: ["./google-login.component.scss"],
})
export class GoogleLoginComponent implements OnInit {
  constructor(private authSvc: AngularFireAuth, private router: Router) {}

  ngOnInit() {}

  doLogin() {
    this.authSvc.auth.signInWithPopup(new auth.GoogleAuthProvider()).then(
      (res) => {
        alert("Login Successfull");
        console.log(res.user);
        this.router.navigate(["/"]);
      },
      (err) => {
        alert("Unable to authenticate using Google");
        console.log(err);
      }
    );
  }
}
