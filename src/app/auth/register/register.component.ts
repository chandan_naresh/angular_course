import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth.service";
import { User } from "src/app/models/user.model";
import { Router } from "@angular/router";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
  model: User = {
    email: "",
    fullName: "",
    isLoggedIn: false,
    mobile: "",
    cart: null,
    password: "",
    type: "",
  };

  userType: string[] = ["seller", "customer"];
  constructor(private authSvc: AuthService, private router: Router) {}

  ngOnInit() {}

  get diagnostic() {
    return JSON.stringify(this.model);
  }

  save() {
    this.authSvc.registerUser(this.model).then(() => {
      this.router.navigate(["/"]);
    });
  }
}
