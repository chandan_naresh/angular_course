import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";

import { FacebookLoginComponent } from "./facebook-login/facebook-login.component";
import { GoogleLoginComponent } from "./google-login/google-login.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AuthRoutingModule } from "./auth-routing.module";
@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ResetPasswordComponent,

    FacebookLoginComponent,
    GoogleLoginComponent,
  ],
  imports: [CommonModule, ReactiveFormsModule, AuthRoutingModule, FormsModule],
  exports: [LoginComponent, RegisterComponent],
})
export class AuthModule {}
