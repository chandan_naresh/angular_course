import { Component, OnInit } from "@angular/core";
import { User } from "../../models/user.model";
import { AngularFireAuth } from "@angular/fire/auth";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../auth.service";
import { error } from "util";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent {
  title: string = "Login";
  _user: User;
  msg: string;
  loginForm: FormGroup;

  constructor(
    private authSvc: AuthService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.loginForm = fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.minLength(6)]],
    });
  }

  doLogin() {
    if (this.loginForm.valid) {
      let data = this.loginForm.value;
      this.authSvc.emailLogin(data.email, data.password).then(
        () => {
          this.msg = "Login Successfull";
          this.router.navigate(["product"]);
        },
        (err) => (this.msg = "invalid credentials")
      );
    } else {
      alert("form is invalid");
    }
  }
}
