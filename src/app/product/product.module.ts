import { NgModule } from "@angular/core";

import { ProductComponent } from "./product.component";
import { ProductListComponent } from "./product-list/product-list.component";
import { ProductFilterComponent } from "./product-filter/product-filter.component";
import { ProductCardComponent } from "./product-card/product-card.component";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    ProductComponent,
    ProductListComponent,
    ProductFilterComponent,
    ProductCardComponent,
  ],
  imports: [CommonModule, ReactiveFormsModule],
  exports: [ProductComponent],
})
export class ProductModule {}
