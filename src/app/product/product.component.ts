import { Component, OnInit, OnDestroy } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { Product } from "../models/product.model";
import { AuthService } from "../auth/auth.service";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.scss"],
})
export class ProductComponent implements OnInit, OnDestroy {
  products: Product[] = [];
  filteredProducts: Product[] = [];
  constructor(private fireDb: AngularFirestore, private authSvc: AuthService) {
    this.fireDb
      .collection("products")
      .valueChanges()
      .subscribe((res: Product[]) => {
        this.products = res;
        this.filteredProducts = res;
      });
  }

  handleCartClicked(event: string) {
    //TODO: Handle how to save data into the cart
    console.log(event);
    console.log(this.authSvc.currentUser);
  }

  filterPrice(price: number) {
    console.log(price);
    this.filteredProducts = this.products.filter((product) => {
      return product.rate < price;
    });
  }
  ngOnInit() {}

  ngOnDestroy() {
    console.log("product component destroy event");
  }
}
