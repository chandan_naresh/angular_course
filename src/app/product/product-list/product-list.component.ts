import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Product } from "src/app/models/product.model";

@Component({
  selector: "app-product-list",
  templateUrl: "./product-list.component.html",
  styleUrls: ["./product-list.component.scss"],
})
export class ProductListComponent implements OnInit {
  @Input() showOutOfStock: boolean;
  _products: Product[];

  @Output() onAddToCart = new EventEmitter<string>();

  @Input()
  public set products(v: Product[]) {
    this._products = v;
    this.printToConsole();
  }

  public get products(): Product[] {
    return this._products;
  }

  printToConsole() {
    console.log(this.products, this.showOutOfStock);
  }

  constructor() {}

  ngOnInit() {}

  buttonClicked(id: string) {
    this.onAddToCart.emit(id);
  }
}
