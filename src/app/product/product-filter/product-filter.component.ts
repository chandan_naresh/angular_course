import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: "app-product-filter",
  templateUrl: "./product-filter.component.html",
  styleUrls: ["./product-filter.component.scss"],
})
export class ProductFilterComponent implements OnInit {
  filterGrp: FormGroup;
  @Output() onPriceFilter = new EventEmitter<number>();
  constructor(private fb: FormBuilder) {
    this.filterGrp = fb.group({
      categories: "",
      price: 0,
      size: "",
    });
  }

  ngOnInit() {}

  onDoneEditing() {
    //console.log(this.filterGrp.controls["price"].value);
    this.onPriceFilter.emit(this.filterGrp.controls["price"].value);
  }
}
