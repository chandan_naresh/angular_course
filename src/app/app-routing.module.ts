import { NgModule } from "@angular/core";
import { Routes, RouterModule, Route } from "@angular/router";

import { ProductComponent } from "./product/product.component";
import { AuthguardService } from "./authguard.service";

const routes: Routes = [
  {
    path: "auth",
    loadChildren: () => import("./auth/auth.module").then((m) => m.AuthModule),
  },

  {
    path: "seller",
    canActivate: [AuthguardService],

    loadChildren: () =>
      import("./seller/seller.module").then((r) => r.SellerModule),
  },
  {
    path: "profile",
    loadChildren: () =>
      import("./profile/profile.module").then((p) => p.ProfileModule),
  },
  {
    path: "product",
    component: ProductComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
