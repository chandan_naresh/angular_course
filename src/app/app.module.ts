import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from "@angular/fire/firestore";

import { AppComponent } from "./app.component";
import { FooterComponent } from "./footer/footer.component";
import { NavbarComponent } from "./navbar/navbar.component";

import { AppRoutingModule } from "./app-routing.module";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { ProductModule } from "./product/product.module";
import { FormsModule } from "@angular/forms";

const firebaseConfig = {
  apiKey: "AIzaSyCShpj5uXNXw6wGBbFG_kKAI2fS3M3kVsg",
  authDomain: "njoyclient.firebaseapp.com",
  databaseURL: "https://njoyclient.firebaseio.com",
  projectId: "njoyclient",
  storageBucket: "njoyclient.appspot.com",
  messagingSenderId: "996309707813",
  appId: "1:996309707813:web:5b8b6e6b9b93633342a3cc",
  measurementId: "G-NH2WKVZWRD",
};

@NgModule({
  declarations: [AppComponent, FooterComponent, NavbarComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    ProductModule,
    FormsModule,
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent],
})
export class AppModule {}
