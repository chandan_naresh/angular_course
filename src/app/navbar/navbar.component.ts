import { Component, OnInit, Input } from "@angular/core";
import { AuthService } from "../auth/auth.service";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent implements OnInit {
  _isLoggedIn: boolean;
  @Input()
  public set isLoggedIn(v: boolean) {
    this._isLoggedIn = v;
  }
  public get isLoggedIn(): boolean {
    return this._isLoggedIn;
  }

  constructor(private authSvc: AuthService) {}

  query: string = "Search your product here";
  ngOnInit() {}

  doLogout() {
    this.authSvc.signout();
  }
}
